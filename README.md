# chargrrd


## Description
A simple daemon for [chargrr-ui](https://gitlab.com/bergnet/public/chargrr/chargrr-ui).

## Installation

## Usage

## Support

## Roadmap
- [ ] Inital release
- [ ] Feed influxdb with data
- [ ] Other TSDB-ingestors
- [ ] Homeassistant integration
- [ ] Docker deployment

## Contributing
TBD

## Authors and acknowledgment

## License
AGPL v3

## Project status
In early stages, not even an alpha, just a thought in the back of everyones mind.
